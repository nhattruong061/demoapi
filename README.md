# DemoApi

- Open project by Visual studio. Wait to restore all Nuget packets.
- Change Connectstring in "appsettings.json"
- Open CMD. cd to project folder.
- run: "dotnet ef migrations add InitialMigration"
- run: "dotnet ef migrations remove" if migration already exist. and rerun 
        "dotnet ef migrations add InitialMigration"