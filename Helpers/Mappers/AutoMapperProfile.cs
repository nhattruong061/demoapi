﻿using AutoMapper;
using DemoAPI.Dtos;
using DemoAPI.Entities;

namespace DemoAPI.Helpers.Mappers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>();
        }
    }
}